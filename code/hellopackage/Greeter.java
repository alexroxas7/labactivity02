package hellopackage;
import java.util.Scanner;

import secondpackage.Utilities;

import java.util.Random;
import java.util.*;

public class Greeter {
    public static void main(String args[]){
        Scanner scanner = new Scanner(System.in);
        Random rand = new Random();
        System.out.println("Enter a number: ");
        int number = scanner.nextInt();
        System.out.println(Utilities.doubleMe(number));
    }
}